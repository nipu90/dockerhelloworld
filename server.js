const express = require('express');

const PORT = 3030;
const app = express();

app.get('/', (req, res) => {
    res.send(`<h1>Hello World</h1>`);
});

app.listen(PORT, ()=> {
    console.log(`Running on http://localhost:${PORT}`);
});